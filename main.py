from sys import stdin, exit
import os, requests, re, time, csv
import logging
from githubstats import GitHubStats

# create logger and configuring handler and formatter
logger = logging.getLogger("githubstats_logger")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)

# reading environment variable for authorization token (if set)
authtoken = ('authorization' in os.environ and os.environ['authorization']) or None

'''
  Method to read input from STDIN.
'''
def read_input():
    githubrepos = []
    print "Welcome to GitHub Stats App"
    print "Please enter repo in each line in format orgname/repo. Once done, press enter to start processing"
    while True:
        userinput = stdin.readline().strip("\n")
        print userinput
        if userinput == "":
            logger.info("Got empty input, will start fetching response")
            break
        else:
            # validating userinput to check format of repo
            match = re.match("([-_\\w]+)\\/([-_.\\w]+)", userinput)
            # if regex matches and there is exactly two groups (one for orgname and one for repo)
            if match is not None and len(match.groups()) == 2:
                githubrepos.append(userinput)
            else:
                logger.error("Invalid input line " + userinput)
    return githubrepos

'''
    method that iterates on gitrepos and call github api to fetch relevant data
'''
def get_github_stats():
    githubrepos = read_input()
    githubstats = []
    is_rate_limit_exceeded = False
    # setting up base url for github api
    github_api_base_url = 'https://api.github.com/repos/%s/%s/commits/master'
    for repo in githubrepos:
        repo_parts = repo.split('/')
        url = github_api_base_url % (repo_parts[0], repo_parts[1])
        response = None
        try:
            # if authtoken is not None then set authorization headers
            if authtoken is not None:
                response = requests.get(url, headers = {'Authorization':'token ' + authtoken})
            else:
                response = requests.get(url)
            # raise error if it is related to http
            response.raise_for_status()
            stats = response.json()
            g = GitHubStats(repo, url, stats['commit']['author']['date'], stats['commit']['author']['name'])
            json_repr = g.toJSON()
            githubstats.append(json_repr)
        except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as err:
            if response is not None and response.status_code == 403:
                rate_limit = response.headers['X-RateLimit-Remaining'] or None
                if rate_limit is not None:
                    reset_epoch_time = response.headers['X-RateLimit-Reset']
                    reset_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(reset_epoch_time)))
                    logger.warn("Rate limit exceeded, please try again on or after: " + reset_time)
                    is_rate_limit_exceeded = True
                    break
            logger.error(err)
            exit(1)
    
    write_csv(is_rate_limit_exceeded, githubstats)

def write_csv(is_rate_limit_exceeded, githubstats):
    timestr = time.strftime("%Y%m%d-%H%M%S")
    csv_file_name = 'csv_report_' + timestr + ".csv"
    if len(githubstats) != 0:
        if is_rate_limit_exceeded:
            logger.info("Rate limited exceeded, however writing the proceesed results in file: " + csv_file_name)
        else:
            logger.info("Writing the processed results in file: " + csv_file_name)
        try:
            with open('reports/' + csv_file_name, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=githubstats[0].keys())
                writer.writeheader()
                for data in githubstats:
                    writer.writerow(data)
                print os.path.abspath(csvfile.name)
            with open('reports/' + csv_file_name) as f:
                content = f.readlines()
            # you may also want to remove whitespace characters like `\n` at the end of each line
            content = [x.strip() for x in content] 
            print content
        except IOError as (errno, strerror):
                logger.error("I/O error({0}): {1}".format(errno, strerror))

    
if __name__== "__main__":
  get_github_stats()