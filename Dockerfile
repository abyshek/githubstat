FROM python:2.7.14
COPY requirements.txt .
COPY *.py /
RUN pip install -U pip setuptools
RUN pip install -r requirements.txt
RUN mkdir -p reports
CMD ["python", "-u", "main.py"]