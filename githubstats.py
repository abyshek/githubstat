import json
class GitHubStats(object):

    def __init__(self, name, url, last_commit_date, latest_author_name):
        self.name = name 
        self.url = url
        self.last_commit_date = last_commit_date
        self.latest_author_name = latest_author_name
    
    def toJSON(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__))