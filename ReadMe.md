## GitHub Stats

#### Problem Statement
Develop an application/script that accepts a list of public github repositories and printout url,latest author name,name and last commit date

#### Solution Details (Demo Video Link: https://streamable.com/gx9os)
- Implemented python script to take list of repos from STDIN. 
- Once read, it iterates on repos line by line and fetch information using GitHub API
- And, it writes to CSV

#### Running Instructions
- The application root directory has dockerfile which can be used to build image to prepare and install dependencies and build docker image. Use command to build docker image from directory where Dockekrfile is: 

    `docker build -t gitstats .` 
    
- Once build, use command to run application:

    `docker run -v ~/reports:/reports -i gitstats`

- On running above command, you can start providing repo name on each line in format (orgname/repo). Alternatively you can also read file to pass the list of repos to the above command as STDIN through pipe operator. Use below command for same (Assuming repos.txt file has repo (orgname/repo) in each line:

    `cat repos.txt | docker run -v ~/reports:/reports -i gitstats`

- Above command will generate a CSV in directory mentioned as volume mount in above command

##### Note:
- As GitHub API has throttling in place, you can only make 60 API calls per hour (as unauthorized user)
- If token is available, we can make more number of calls which is around 5000 API calls per hour.

The provided solution covers both the above mentioned aspect. To make authenticated requests, one has to provide personal access token that can be used to fetch information about the repositories as authenticated user. One can create his/her own personal access token following this link: (https://github.com/settings/tokens). Make sure you select right scopes while creating your token. Once you have the token you can pass on your personal access token as an environment variable to the script (as shown below):

`cat repos.txt | docker run -e authorization=<your_personal_access_token> -v ~/reports:/reports -i gitstats`